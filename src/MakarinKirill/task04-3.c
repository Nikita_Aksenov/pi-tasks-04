#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 4

void readStr(char (*arr)[256])
{
    int i;
    for (i = 0; i < MAX; i++)
        fgets(arr+i, 256, stdin);
}

void lenStr(char (*arr)[256], int *len, char *num[])
{
    int i;
    for (i = 0; i < MAX; i++)
    {
        len[i] = strlen(arr+i)-1;
        num[i] = arr+i;
    }
}

void sortLen(int *len, char *num[])
{
    int i, j;
    int temp;
    char *t;
    
    for (i = 0; i < MAX-1; i++)
        for (j = 0; j < MAX-i-1; j++)
            if (len[j] > len[j+1])
            {
                // меняем местами значени длин
                temp = len[j];
                len[j] = len[j+1];
                len[j+1] = temp;
                // меянем местами указатели на строки
                t = num[j];
                num[j] = num[j+1];
                num[j+1] = t;
            }
}

void outStr(char *num[])
{
    int i;
    for (i = 0; i < MAX; i++)
        puts(num[i]);
}

int main()
{
    char str[MAX][256];
    int strLen[MAX] = { 0 };
    char *NumOfStr[MAX];
    
    readStr(str);
    lenStr(str, strLen, NumOfStr);
    sortLen(strLen, NumOfStr);
    outStr(NumOfStr);
    
    return 0;
}
