#include <stdio.h>
#define MAX 4

void enterLine(char *arr[])
{
    int i;
    for( i = 0; i < MAX; i++)
        fgets(arr+i, 256, stdin);
}

void outLine (char *arr[])
{
    int i;
    for(i = (MAX-1); i >= 0; i--)
        puts(arr+i);
}

int main()
{
    char str[MAX][256] = {0};
    
    printf("Please, enter %d line\n", MAX);
    
    enterLine(str);
    outLine(str);
    
    return 0;
}
