#include <stdio.h>
#include <string.h>
#define N 256
#define SIZE 5		//quantity of strings

void LenSort(char (*string)[N])
{
	int i, j, MinLen=N, MinIndex=0;
	printf("\nLines sorted by length:\n");
	for (i=0; i<SIZE; i++)
	{
		for (j=0; j<SIZE; j++)
			if ((strlen(string[j]) < MinLen) && (string[j][0]!=0))
			{
				MinLen=strlen(string[j]);
				MinIndex=j;
			}
		printf("%d) %s", i+1, string[MinIndex]);
		string[MinIndex][0]=0;
		MinLen=N;
	}
}

int main()
{
	char string[SIZE][N];
	int i;
	printf("Enter %d strings:\n",SIZE);
	for (i=0; i<SIZE; i++)
		fgets(string[i],N,stdin);
	LenSort(string);
	return 0;
}
