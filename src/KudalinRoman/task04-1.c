#define _CRT_SECURE_NO_WARNINGS
#define MAX 20
#include <stdio.h>
char str[MAX][256] = {'\0'};
int i=0;
void createArr()
{
	for (i = 0; i < MAX; i++)
	{
		fgets(str[i], 256, stdin);
		str[i][strlen(str[i])-1]=0;
		if (str[i][0] == '\0')
			break;
	}
}
void reverseArr()
{
	for (--i; i>=0; i--)
		puts(str[i]);
}
int main()
{
	puts("Enter your strings, please. You can enter 20 strings max.");
	createArr();
	puts("\nReversed Strings:");
	reverseArr();
	return 0;
}