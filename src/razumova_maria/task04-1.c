#include <stdio.h>
#include <string.h>
#define N 10
#define L 20

int enterStrings(char (*s)[L])
{
    int i=0;
    for(i=0;i<N;i++)
    {
        fgets(s[i],L,stdin);
        s[i][strlen(s[i])-1]='\0';
        if (strlen(s[i])==0)
            return i;
    }
    return i;
}
void printStrings(char(*s)[L],int kol)
{
    int i;
    for(i=0;i<kol;i++)
    {
        printf("%s\n",s[i]);
    }
}
void printAround(char(*s)[L], int kol)
{
    int i;
    for(i=(kol-1);i>=0;i--)
    {
        printf("%s\n",s[i]);
    }
}

int main(void)
{
    char strings[N][L];
    int kol;
    kol=enterStrings(strings[0]);
    printAround(strings[0],kol);
    return 0;
}
