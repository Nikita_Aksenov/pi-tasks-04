#define N 5
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main()
{
    srand(time(0));
    char string[N][256];
    int i = 0, j = 0;
    int strcount;
	printf("Enter a string:\n");
    while (i < N)
    {
        fgets(string[i], 256, stdin);
        i++;
    }
    strcount = rand() % (i - 1);
    puts("");
	printf("Random string:\n");
    while (string[strcount][j] > 10)
    {
        printf("%c",string[strcount][j]);
        j++;
    }
    puts("");
    return 0;
}
