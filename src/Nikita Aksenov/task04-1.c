#define N 5
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
int main()
{
	int i, j, length;
	char str[N][256];
	printf("Enter the string\n" );
	for(i = 0; i < N; i++)		
		fgets(str[i], 256, stdin);
	printf("\n");
	printf("Reverse order\n" );
	for(i = N-1; i > -1; i--)
	{
		length = strlen(str[i]) - 1;					
		for(j = 0; j < length; j++)
		printf("%c",str[i][j]);
	printf("\n");
	}
	return 0;
}
